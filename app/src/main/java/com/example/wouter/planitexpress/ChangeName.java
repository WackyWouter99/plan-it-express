package com.example.wouter.planitexpress;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;


public class ChangeName extends AppCompatActivity {

//    initiate variables
    EditText setName;
    Button editName;
    String username;
    String setNameV;
    String insertUrl = "http://pascallenting.nl/wchangeName.php";
    RequestQueue requestQueue;
    SharedPreferences user;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_name);

        user = PreferenceManager.getDefaultSharedPreferences(this);

//        declare variables
        setName = (EditText) findViewById(R.id.setName);
        editName = (Button) findViewById(R.id.editName);
        requestQueue = Volley.newRequestQueue(getApplicationContext());
        username = user.getString("username", "Could not retrieve username");

        editName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                sla input op
                setNameV = setName.getText().toString();

//               check of het ingevuld is
                if(TextUtils.isEmpty(setNameV)) {
                    setName.setError("You cannot leave this empty!");

                }
//                check of hij niet te lang is
                else if (setNameV.length() > 30){
                    setName.setError("Too long!");
                }
//                update name in database
                else{
                    StringRequest stringRequest = new StringRequest(Request.Method.POST, insertUrl, new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Toast.makeText(getApplicationContext(), "Name Changed", Toast.LENGTH_SHORT).show();
//                            update name in shared preference
                            editor = user.edit();
                            editor.putString("name", setNameV);
                            editor.apply();
                            Intent intent = new Intent(getApplicationContext(), Account.class);
                            startActivity(intent);
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
//                            display error
                            String errorText = "Error: \n\n" + error.toString();
//                           make toast met error
                            Toast.makeText(getApplicationContext(), errorText, Toast.LENGTH_LONG).show();
                            error.printStackTrace();
                        }
                    }){
                        @Override
                        protected Map<String, String> getParams()
                        {
                            Map<String, String> params = new HashMap<String, String>();
                            params.put("username",username);
                            params.put("name",setNameV);
                            return params;
                        }
                    }; requestQueue.add(stringRequest);
//                  close activity
                    finish();

                }
            }
        });
    }
}
