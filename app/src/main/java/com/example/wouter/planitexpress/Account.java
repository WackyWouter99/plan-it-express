package com.example.wouter.planitexpress;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class Account extends AppCompatActivity {

//    initiate variables
    TextView usernameView;
    TextView nameView;
    Button editPw;
    Button editName;
    SharedPreferences user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account);
//        get users name en username
        user = PreferenceManager.getDefaultSharedPreferences(this);
        String name = user.getString("name", "Could not retrieve name");
        String username = user.getString("username", "Could not retrieve username");



//        declare variables
        usernameView = (TextView) findViewById(R.id.username);
        nameView = (TextView) findViewById(R.id.name);
        editName = (Button) findViewById(R.id.editName);
        editPw = (Button) findViewById(R.id.editPw);

//      fill in username view en name view
        usernameView.setText(username);
        nameView.setText(name);


//        ga naar editpw activity na klik op knop
        editPw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), ChangePw.class);
                startActivity(intent);
                finish();
            }
        });
//        ga naar editpw activity na klik op knop
        editName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), ChangeName.class);
                startActivity(intent);
                finish();
            }
        });

    }
}
