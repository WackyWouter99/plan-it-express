package com.example.wouter.planitexpress;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;


public class ChangePw extends AppCompatActivity {
//  initiate variables
    EditText setPw;
    EditText setConfirmPw;
    Button editPw;
    String username;
    String insertUrl = "http://pascallenting.nl/wchangePw.php";
    RequestQueue requestQueue;
    String setPwV;
    String setConfirmPwV;
    SharedPreferences user;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_pw);

        user = PreferenceManager.getDefaultSharedPreferences(this);
//      declare variables
        setPw = (EditText) findViewById(R.id.setPw);
        setConfirmPw = (EditText) findViewById(R.id.setConfirmPw);
        editPw = (Button) findViewById(R.id.editPw);
        requestQueue = Volley.newRequestQueue(getApplicationContext());
        username = user.getString("username", "Could not retrieve username");

        editPw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//              sla inputs op
                setPwV = setPw.getText().toString();
                setConfirmPwV = setConfirmPw.getText().toString();
//                check of de inputs leeg zijn
                if(TextUtils.isEmpty(setPwV)) {
                    setPw.setError("You cannot leave this empty!");

                }
//
                else if(TextUtils.isEmpty(setConfirmPwV)) {
                    setConfirmPw.setError("You cannot leave this empty!");

                }
//                check of ze overeenkomen
                else if(!setPwV.equals(setConfirmPwV)){
                    setPw.setError("This doesn't match");
                    setConfirmPw.setError("This doesn't match");
                }
                else if (setPwV.length() > 30){
                    setPw.setError("Too long!");
                }
//                update password in database
                else{
                    StringRequest stringRequest = new StringRequest(Request.Method.POST, insertUrl, new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Toast.makeText(getApplicationContext(), "Password Changed", Toast.LENGTH_SHORT).show();
//                            update shared preference
                            editor = user.edit();
                            editor.putString("password", setPwV);
                            editor.apply();
//                            back to account
                            Intent intent = new Intent(getApplicationContext(), Account.class);
                            startActivity(intent);

                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            String errorText = "Error: \n\n" + error.toString();
//                           make toast met error
                            Toast.makeText(getApplicationContext(), errorText, Toast.LENGTH_LONG).show();
                            error.printStackTrace();

                        }
                    }){
                        @Override
                        protected Map<String, String> getParams()
                        {
                            Map<String, String> params = new HashMap<String, String>();
                            params.put("username",username);
                            params.put("password",setPwV);
                            return params;
                        }
                    }; requestQueue.add(stringRequest);
//                  close activity
                    finish();
                }
            }
        });
    }
}
