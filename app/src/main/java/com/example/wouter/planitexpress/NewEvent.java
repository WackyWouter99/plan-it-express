package com.example.wouter.planitexpress;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;


public class NewEvent extends AppCompatActivity {

    Button newEvent;
    ImageButton dateButton;
    EditText nameEvent;
    EditText description;
    EditText date;
    String username;
    SharedPreferences user;
    String insertUrl = "http://pascallenting.nl/winsertEvent.php";
    String nameEventValue;
    String descriptionValue;
    String dateValue;
    RequestQueue requestQueue;
    boolean rightLength;
    boolean notEmpty;
    boolean failure;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_event);

        newEvent = findViewById(R.id.newEventBtn);
        dateButton = findViewById(R.id.dateButton);
        nameEvent = findViewById(R.id.nameEvent);
        description = findViewById(R.id.description);
        date = findViewById(R.id.date);



        requestQueue = Volley.newRequestQueue(getApplicationContext());
        user = PreferenceManager.getDefaultSharedPreferences(this);

        username = user.getString("username", "Could not retrieve username");

        newEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                nameEventValue = nameEvent.getText().toString();
                descriptionValue = description.getText().toString();
                dateValue =  fixDate(date.getText().toString());
                if(dateValue.equals("failure")){
                    failure = true;
                    date.setError("Pls write date like this: 05/01/1999");
                }
                else{
                    failure = false;
                }

                String[] inputsV = {nameEventValue, descriptionValue, dateValue};
                EditText[] inputs = {nameEvent,description,date};
                int[] maxLength = {30, 100, 10};

//                loop door inputs om te checken of iets leeg is.
                for(int i = 0; i < inputsV.length; i++){
                    if(TextUtils.isEmpty(inputsV[i])) {

                        notEmpty = false;
                        inputs[i].setError("You cannot leave this empty!");

                        break;

                    }
                    else{
                        notEmpty = true;


                    }

                }

                for(int i = 0; i < inputsV.length; i++){
                    if(inputsV[i].length() <= maxLength[i]) {
                        rightLength = true;

                    }
                    else{
                        inputs[i].setError("Too long");
                        rightLength = false;

                        break;
                    }

                }

                if( notEmpty && rightLength && !failure) {
                    StringRequest stringRequest = new StringRequest(Request.Method.POST, insertUrl, new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Toast.makeText(getApplicationContext(), "Succes", Toast.LENGTH_SHORT).show();


                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            String errorText = "Error: \n\n" + error.toString();

                            //                           make toast met error
                            Toast.makeText(getApplicationContext(), errorText, Toast.LENGTH_LONG).show();
                        }
                    }) {
                        @Override
                        protected Map<String, String> getParams() {
                            Map<String, String> params = new HashMap<String, String>();
                            params.put("name", nameEventValue);
                            params.put("date", dateValue);
                            params.put("owner", username);
                            params.put("leden", username + ",");
                            params.put("description", descriptionValue);
                            params.put("cancelled", "");
                            return params;
                        }
                    };
                    requestQueue.add(stringRequest);
                    finish();


                    Intent intent = new Intent(getApplicationContext(), Homescreen.class);
                    startActivity(intent);
                }
            }
        });


        dateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getApplicationContext(), PieChartActivity.class);
                startActivity(intent);

            }
        });
    }
    private String fixDate(String date){
        int digits = 0;
        String result = "";
        for (int i = 0; i < date.length(); i++){

            char a_char = date.charAt(i);
            if(i == 2 || i == 5){
                if (date.charAt(i)!= '/'){
                    result += '/';
                }
                else{
                    result += date.charAt(i);
                }

            }
           else {
                if (!Character.isDigit(date.charAt(i))){
                    return "failure";

                }
                else{
                 result += date.charAt(i);
                }

            }

        }

        return result;
    }
}
