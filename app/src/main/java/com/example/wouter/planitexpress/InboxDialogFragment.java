package com.example.wouter.planitexpress;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

public class InboxDialogFragment extends DialogFragment{

    String insertUrl = "http://pascallenting.nl/waddMember.php";
    String cancelUrl = "http://pascallenting.nl/waddCancelled.php";
    RequestQueue requestQueue;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        requestQueue = Volley.newRequestQueue(getActivity());
        Bundle nameBundle = getArguments();
        final String eventName = nameBundle.getString("name");
        final String username = nameBundle.getString("username");

        builder.setMessage("Do you want to attend this event?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        StringRequest stringRequest = new StringRequest(Request.Method.POST, insertUrl, new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {


                                                         }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                String errorText = "Error: \n\n" + error.toString();
                                error.printStackTrace();

                            }
                        }){
                            @Override
                            protected Map<String, String> getParams()
                            {
                                Map<String, String> params = new HashMap<String, String>();
                                params.put("member", username);
                                params.put("name", eventName);
                                return params;
                            }
                        }; requestQueue.add(stringRequest);


                    }

                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        StringRequest stringRequest = new StringRequest(Request.Method.POST, cancelUrl, new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {


                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                String errorText = "Error: \n\n" + error.toString();
                                error.printStackTrace();

                            }
                        }){
                            @Override
                            protected Map<String, String> getParams()
                            {
                                Map<String, String> params = new HashMap<String, String>();
                                params.put("member", username);
                                params.put("name", eventName);
                                return params;
                            }
                        }; requestQueue.add(stringRequest);

                    }
                })
                .setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                    }

        });

        // Create the AlertDialog object and return it
        return builder.create();
    }



}
