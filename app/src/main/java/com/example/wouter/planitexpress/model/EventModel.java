package com.example.wouter.planitexpress.model;

import java.io.Serializable;

public class EventModel implements Serializable {
//  initiate var
    private String name;
    private String description;
    private String date;
    private String owner;
    private String leden;

//  constructor
    public EventModel(String name, String description, String date, String owner, String leden){
        this.name = name;
        this.description = description;
        this.date = date;
        this.owner = owner;
        this.leden = leden;
    }

    public String getName(){
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getDate(){
        return date;
    }

    public String getOwner(){
        return owner;
    }

    public EventModel(){

    }
}
