package com.example.wouter.planitexpress;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;



public class PieChartActivity extends AppCompatActivity {

    private PieChart mChart;
    String getUrl = "http://pascallenting.nl/wshowEvents.php";
    RequestQueue requestQueue;
    int[] months = new int[12];


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pie_chart);

        mChart = (PieChart) findViewById(R.id.chart);
        mChart.setDescription("");
        mChart.setTouchEnabled(false);
        mChart.setDrawSliceText(true);
        mChart.getLegend().setEnabled(false);
        mChart.setTransparentCircleColor(Color.rgb(130, 130, 130));
        mChart.animateY(1400, Easing.EasingOption.EaseInOutQuad);
        mChart.setDescriptionTextSize(15);

        requestQueue = Volley.newRequestQueue(getApplicationContext());

        setData();


    }

    private void setData() {

        final ArrayList<Entry> yValues = new ArrayList<>();
        final ArrayList<String> xValues = new ArrayList<>();

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, getUrl, (String) null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONArray events = response.getJSONArray("events");
                    Arrays.fill(months, 0);

                    for (int i = 0; i < events.length(); i++) {

                        JSONObject event = events.getJSONObject(i);

                        String date = event.getString("date");


                        if(date.matches("(.*)/01/(.*)")) {
                            months[0] += 1;
                        } else if(date.matches("(.*)/02/(.*)")){
                            months[1] += 1;
                        } else if(date.matches("(.*)/03/(.*)")){
                            months[2] += 1;
                        } else if(date.matches("(.*)/04/(.*)")){
                            months[3] += 1;
                        } else if(date.matches("(.*)/05/(.*)")){
                            months[4] += 1;
                        } else if(date.matches("(.*)/06/(.*)")){
                            months[5] += 1;
                        } else if(date.matches("(.*)/07/(.*)")){
                            months[6] += 1;
                        } else if(date.matches("(.*)/08/(.*)")){
                            Log.d("idd", "dit doe ik");
                            months[7] += 1;
                        } else if(date.matches("(.*)/09/(.*)")){
                            months[8] += 1;
                        } else if(date.matches("(.*)/10/(.*)")){
                            months[9] += 1;
                        } else if(date.matches("(.*)/11/(.*)")){
                            months[10] += 1;
                        } else if(date.matches("(.*)/12/(.*)")){
                            months[11] += 1;
                        }


                    }

                    if(months[0] > 0) {
                        yValues.add(new Entry(months[0], 0));
                        xValues.add("January");
                    }

                    if(months[1] > 0) {
                        yValues.add(new Entry(months[1], 1));
                        xValues.add("February");
                    }

                    if(months[2] > 0) {
                        yValues.add(new Entry(months[2], 2));
                        xValues.add("March");
                    }

                    if(months[3] > 0) {
                        yValues.add(new Entry(months[3], 3));
                        xValues.add("April");
                    }

                    if(months[4] > 0) {
                        yValues.add(new Entry(months[4], 4));
                        xValues.add("May");
                    }

                    if(months[5] > 0) {
                        yValues.add(new Entry(months[5], 5));
                        xValues.add("June");
                    }

                    if(months[6] > 0) {
                        yValues.add(new Entry(months[6], 6));
                        xValues.add("July");
                    }

                    if(months[7] > 0) {
                        yValues.add(new Entry(months[7], 7));
                        xValues.add("August");
                    }
                    if(months[8] > 0) {
                        yValues.add(new Entry(months[8], 8));
                        xValues.add("September");
                    }
                    if(months[9] > 0) {
                        yValues.add(new Entry(months[9], 9));
                        xValues.add("October");
                    }
                    if(months[10] > 0) {
                        yValues.add(new Entry(months[10], 10));
                        xValues.add("November");
                    }
                    if(months[11] > 0) {
                        yValues.add(new Entry(months[11], 11));
                        xValues.add("December");
                    }


                    //  http://www.materialui.co/colors
                    ArrayList<Integer> colors = new ArrayList<>();
                    colors.add(Color.rgb(244,67,54));
                    colors.add(Color.rgb(233,30,99));
                    colors.add(Color.rgb(156,39,176));
                    colors.add(Color.rgb(63,81,181));
                    colors.add(Color.rgb(33,150,243));
                    colors.add(Color.rgb(0,188,212));
                    colors.add(Color.rgb(76,175,80));
                    colors.add(Color.rgb(255,235,59));
                    colors.add(Color.rgb(255,152,0));
                    colors.add(Color.rgb(96,125,139));
                    colors.add(Color.rgb(158,158,158));
                    colors.add(Color.rgb(121,85,72));
                    PieDataSet dataSet = new PieDataSet(yValues, "Months");
                    dataSet.setColors(colors);
                    dataSet.setValueTextSize(15);

                    PieData data = new PieData(xValues, dataSet);
                    mChart.setData(data); // bind dataset aan chart.
                    mChart.invalidate();  // Aanroepen van een redraw


                } catch (Exception e) {

                }

            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String errorText = "Error: \n\n" + error.toString();
//                        make toast met error
                Toast.makeText(getApplicationContext(), errorText, Toast.LENGTH_LONG).show();

            }
        }); requestQueue.add(jsonObjectRequest);






    }



}



