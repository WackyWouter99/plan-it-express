package com.example.wouter.planitexpress;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;


public class LoginActivity extends AppCompatActivity {

//    initiate var
    Button register;
    Button login;
    EditText username;
    EditText password;
    TextView errorView;
    String usernameValue;
    String passwordValue;
    SharedPreferences user;
    private String usernameV;
    private String passwordV;
    private int idV;
    private String nameV;
    SharedPreferences.Editor editor;


    RequestQueue requestQueue;
    String getUrl = "http://pascallenting.nl/wshowUsers.php";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

//        declare var

        requestQueue = Volley.newRequestQueue(getApplicationContext());

        register = (Button) findViewById(R.id.register);
        login = (Button) findViewById(R.id.login);
        username = (EditText) findViewById(R.id.username);
        password = (EditText) findViewById(R.id.password);
        user = PreferenceManager.getDefaultSharedPreferences(this);
        editor = user.edit();

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                sla inputs op
                usernameValue = username.getText().toString();
                passwordValue = password.getText().toString();


                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, getUrl, (String) null, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try{

                            JSONArray users = response.getJSONArray("users");
                            boolean loggedIn = false;


                            for(int i =0; i < users.length(); i ++)
                            {
                                JSONObject user = users.getJSONObject(i);


                                usernameV = user.getString("username");
                                passwordV = user.getString("password");
                                idV = user.getInt("id");
                                nameV = user.getString("name");






//                                check of the credentials are valid
                                if (usernameValue.equals(usernameV) && passwordValue.equals(passwordV)){
                                    Intent intent = new Intent(getApplicationContext(), Homescreen.class);
                                    startActivity(intent);
                                    loggedIn = true;
                                    break;
                                }
                                else{
                                    loggedIn = false;
                                }
                            }

                            if (loggedIn == false){
                                Toast.makeText(getApplicationContext(), "Input valid credentials", Toast.LENGTH_SHORT).show();
                            }else {
                                editor.putString("username", usernameV);
                                editor.putString("password", passwordV);
                                editor.putInt("id", idV);
                                editor.putString("name", nameV);
                                editor.apply();
                            }
                        }catch(Exception e)
                        {

                        }

                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        String errorText = "Error: \n\n" + error.toString();
//                        make toast met error
                        Toast.makeText(getApplicationContext(), errorText, Toast.LENGTH_LONG).show();

                    }
                }); requestQueue.add(jsonObjectRequest);
//


            }
        });
//        start nieuw activity
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), Register.class);
                startActivity(intent);
            }
        });

    }


}
