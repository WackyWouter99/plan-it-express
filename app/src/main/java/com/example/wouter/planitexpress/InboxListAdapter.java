package com.example.wouter.planitexpress;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.wouter.planitexpress.model.EventModel;

import java.util.List;

public class InboxListAdapter extends ArrayAdapter<EventModel> {
    public InboxListAdapter(Context context, int resource, List<EventModel> objects){
        super(context, resource, objects);
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        InboxListAdapter.ViewHolder vh;

        if (convertView == null ) {
            vh = new InboxListAdapter.ViewHolder();
            LayoutInflater li = LayoutInflater.from(getContext());
            convertView = li.inflate(R.layout.activity_event_list_adapter, parent, false);
            vh.name = (TextView) convertView.findViewById(R.id.name);
            vh.date = (TextView) convertView.findViewById(R.id.date);
            vh.description = (TextView) convertView.findViewById(R.id.description);
            vh.owner = (TextView) convertView.findViewById(R.id.owner);
            convertView.setTag(vh);
        } else {
            vh = (InboxListAdapter.ViewHolder) convertView.getTag();
        }
        EventModel cm = getItem(position);
        vh.name.setText((CharSequence) cm.getName());
        vh.date.setText((CharSequence) cm.getDate());
        vh.description.setText((CharSequence) cm.getDescription());
        vh.owner.setText((CharSequence) cm.getOwner());
        return convertView;
    }

    private static class ViewHolder {
        TextView name;
        TextView date;
        TextView description;
        TextView owner;
    }

}
