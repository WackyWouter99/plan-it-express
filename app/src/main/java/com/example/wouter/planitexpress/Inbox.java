package com.example.wouter.planitexpress;


import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.wouter.planitexpress.model.EventModel;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class Inbox extends AppCompatActivity {

    private ListView mListView;
    private EventListAdapter mAdapter;
    private List<EventModel> eventModels = new ArrayList<>();
    String username;
    SharedPreferences user;
    String getUrl = "http://pascallenting.nl/wshowEvents.php";
    RequestQueue requestQueue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inbox);
        requestQueue = Volley.newRequestQueue(getApplicationContext());
        user = PreferenceManager.getDefaultSharedPreferences(this);
        username = user.getString("username", "Could not retrieve username");

        mListView = (ListView) findViewById(R.id.my_list_view);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                             @Override
                                             public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                                                 InboxDialogFragment popUp = new InboxDialogFragment();
                                                 EventModel item = (EventModel)adapterView.getItemAtPosition(position);
                                                 String currentEvent = item.getName();
                                                 Bundle eventName = new Bundle();
                                                 eventName.putString("name", currentEvent);
                                                 eventName.putString("username", username);
                                                 popUp.setArguments(eventName);

                                                 popUp.show(getFragmentManager(),"popUp");


                                             }
                                         }
        );



        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, getUrl, (String) null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONArray events = response.getJSONArray("events");


                    for (int i = 0; i < events.length(); i++) {

                        JSONObject event = events.getJSONObject(i);
                        String lid = event.getString("leden");
                        List<String> ledenLijst = Arrays.asList(lid.split(","));
                        String cancelLid = event.getString("cancelled");
                        List<String> cancelLedenLijst = Arrays.asList(cancelLid.split(","));

                        if(!ledenLijst.contains(username) && !cancelLedenLijst.contains(username)) {
                            String name = event.getString("name");
                            String date = event.getString("date");
                            String description = event.getString("description");
                            String owner = event.getString("owner");

                            eventModels.add(new EventModel(name, description, date, owner, ""));



                        }

                        mAdapter = new EventListAdapter(Inbox.this, 0, eventModels);
                        mListView.setAdapter(mAdapter);


                    }
                } catch (Exception e) {

                }

            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String errorText = "Error: \n\n" + error.toString();
//                        make toast met error
                Toast.makeText(getApplicationContext(), errorText, Toast.LENGTH_LONG).show();

            }
        }); requestQueue.add(jsonObjectRequest);


    }



}




