package com.example.wouter.planitexpress;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

public class Register extends AppCompatActivity {

//    initiate variables
    EditText username;
    EditText fullName;
    EditText password;
    EditText confirmPw;
    String usernameV;
    String fullNameV;
    String passwordV;
    String confirmPwV;
    Button register;
    boolean validpw;
    String insertUrl = "http://pascallenting.nl/winsert.php";
    RequestQueue requestQueue;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

//        declare vars
        username = (EditText) findViewById(R.id.username);
        fullName = (EditText) findViewById(R.id.fullName);
        password = (EditText) findViewById(R.id.password);
        confirmPw = (EditText) findViewById(R.id.confirmPw);
        register = (Button) findViewById(R.id.register);
        requestQueue = Volley.newRequestQueue(getApplicationContext());



        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                get inputs
                usernameV = username.getText().toString();
                fullNameV = fullName.getText().toString();
                passwordV = password.getText().toString();
                confirmPwV = confirmPw.getText().toString();
                String[] inputsV = {usernameV, fullNameV, passwordV, confirmPwV};
                EditText[] inputs = {username, fullName, password, confirmPw};
                int[] maxLength = {16, 30, 25, 25};
                boolean notEmpty = false;
                boolean rightLength = false;
                boolean validpasswords = false;

//                loop door inputs om te checken of iets leeg is.
                for(int i = 0; i < inputsV.length; i++){
                    if(TextUtils.isEmpty(inputsV[i])) {

                        notEmpty = false;
                        inputs[i].setError("You cannot leave this empty!");

                        break;

                    }
                    else{
                        notEmpty = true;


                    }

                }
                for(int i = 0; i < inputsV.length; i++){
                    if(inputsV[i].length() <= maxLength[i]) {
                        rightLength = true;

                    }
                    else{
                        inputs[i].setError("Too long");
                        rightLength = false;

                        break;
                    }

                }

                if (passwordV.equals(confirmPwV)){
                    validpasswords = true;

                }
                else {
                    validpasswords = false;

                    password.setError("The passwords dont match");
                }




                if (validpasswords && notEmpty && rightLength){
                    validpw = true;

                    StringRequest stringRequest = new StringRequest(Request.Method.POST, insertUrl, new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {



                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            String errorText = "Error: \n\n" + error.toString();

//                           make toast met error
                            Toast.makeText(getApplicationContext(), errorText, Toast.LENGTH_LONG).show();
                        }
                    }){
                        @Override
                        protected Map<String, String> getParams()
                        {
                            Map<String, String> params = new HashMap<String, String>();
                            params.put("username",usernameV);
                            params.put("name",fullNameV);
                            params.put("password",passwordV);
                            return params;
                        }
                    }; requestQueue.add(stringRequest);
                    finish();

                }

            }
        });

    }
}
