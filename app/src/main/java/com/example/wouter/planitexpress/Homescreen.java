package com.example.wouter.planitexpress;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

public class Homescreen extends AppCompatActivity {
//    initiate variables
    LinearLayout inbox;
    LinearLayout myEvents;
    LinearLayout newEvent;
    LinearLayout account;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_homescreen);

//        declare variables
        inbox = (LinearLayout) findViewById(R.id.inbox);
        myEvents = (LinearLayout) findViewById(R.id.myEvents);
        newEvent = (LinearLayout) findViewById(R.id.newEvent);
        account = (LinearLayout) findViewById(R.id.account);

//        click listeners die elk naar een nieuwe activity gaan
        inbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), Inbox.class);
                startActivity(intent);
            }
        });

        myEvents.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), MyEvents.class);
                startActivity(intent);
            }
        });

        newEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), NewEvent.class);
                startActivity(intent);
            }
        });

        account.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), Account.class);
                startActivity(intent);
            }
        });


    }
}
